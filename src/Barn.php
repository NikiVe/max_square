<?php


namespace MaxSquare;


class Barn
{
    protected $n = 10;
    protected $m = 10;
    protected array $map;

    public function __construct() {
        $this->rand();
        $this->printMapNM($this->map, $this->n, $this->m);
    }

    public function getHeight() {
        $line = [];
        for ($y = 0; $y < $this->n; $y++) {
            $line = $this->getHeightLine($y, $line);
            $this->printMapN($line, $this->n);
        }
        echo PHP_EOL;
    }

    public function getWidth() {
        $line = [];
        $weight = [];
        for ($y = 0; $y < $this->n; $y++) {
            $line = $this->getHeightLine($y, $line);
            $r = $this->getRight($line);
            $l = $this->getLeft($line);
            for ($x = 0; $x < $this->m; $x++) {
                $weight[$x] = $r[$x] - $l[$x] + 1;
            }
            $this->printMapN($weight, $this->m);
        }
        echo PHP_EOL;
    }

    protected function getHeightLine(int $y, array $line) {
        for ($x = 0; $x < $this->n; $x++) {
            if ($this->map[$x][$y] === 1) {
                $line[$x] = 0;
            } else {
                $line[$x] = isset($line[$x]) ? $line[$x] + 1 : 1;
            }
        }
        return $line;
    }

    protected function getRight(array $line) {
        $r = [];
        $stack = new Stack();
        for ($j = 0; $j < $this->n; $j++) {
            while (!$stack->isEmpty()) {
                if ($line[$j] < $line[$stack->pick()]) {
                    $r[$stack->pop()] = $j - 1;
                } else {
                    break;
                }
            }
            $stack->push($j);
        }
        while (!$stack->isEmpty()) {
            $r[$stack->pop()] = $this->n - 1;
        }
        return $r;
    }

    protected function getLeft(array $line) {
        $l = [];
        $stack = new Stack();
        for ($j = $this->n - 1; $j >= 0; $j--) {
            while (!$stack->isEmpty()) {
                if ($line[$j] < $line[$stack->pick()]) {
                    $l[$stack->pop()] = $j + 1;
                } else {
                    break;
                }
            }
            $stack->push($j);
        }
        while (!$stack->isEmpty()) {
            $l[$stack->pop()] = 0;
        }
        return $l;
    }

    protected function rand() {
        srand(1234);
        for ($i = 0; $i < $this->n; $i++) {
            for ($j = 0; $j < $this->m; $j++) {
                $this->map[$i][$j] = (int)(rand(0, 5) / 4);
            }
        }
    }

    protected function printMapNM(array $array, $n, $m) {
        for ($j = 0; $j < $n; $j++) {
            $str = '';
            for ($i = 0; $i < $m; $i++) {
                $str .= $array[$i][$j] . ' ';
            }
            echo $str . PHP_EOL;
        }
        echo PHP_EOL;
    }

    protected function printMapN(array $array, $n) {
        $str = '';
        for ($i = 0; $i < $n; $i++) {
            $str .= $array[$i] . ' ';
        }
        echo $str . PHP_EOL;
    }
}