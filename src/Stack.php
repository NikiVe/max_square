<?php


namespace MaxSquare;


class Stack
{
    protected $stack = [];
    protected $count = -1;

    public function push($value) {
        $this->stack[++$this->count] = $value;
    }

    public function pop() {
        $value = $this->pick();
        unset($this->stack[$this->count--]);
        return $value;
    }

    public function pick() {
        return $this->stack[$this->count];
    }

    public function isEmpty() {
        return $this->count === -1;
    }
}