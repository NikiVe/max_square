<?php

namespace MaxSquare;

class SolutionN3
{
    protected array $map = [];
    protected int $n = 10;
    protected int $m = 10;

    public function __construct() {
        $this->rand();
        $this->printMap();

        $sT = microtime(true);

        $r = $this->calc();

        echo 'result: ' . $r . PHP_EOL;
        echo 'time: ' . round(microtime(true) - $sT, 4) . PHP_EOL;

    }

    protected function calc() {
        $maxSquare = 0;
        $line = [];
        for ($y = 0; $y < $this->n; $y++) {
            $line = $this->getHeightLine($y, $line);
            for ($x = 0; $x < $this->m; $x++) {
                $square = $this->getMaxSquare($x, $y, $line);
                if ($maxSquare < $square) {
                    $maxSquare = $square;
                }
            }
        }
        return $maxSquare;
    }

    protected function getMaxSquare(int $x, int $y, array $line) {
        $height = -1;
        $maxSquare = 0;
        for ($width = 1; $x + $width <= $this->n; $width++) {
            $h = $line[$x + $width - 1];
            if ($h === 0) {
                break;
            }
            if ($height > $h || $height === -1) {
                $height = $h;
            }
            $square = $width * $height;
            if ($maxSquare < $square) {
                $maxSquare = $square;
            }
        }
        return $maxSquare;
    }

    protected function getHeightLine(int $y, array $line) {
        for ($x = 0; $x < $this->n; $x++) {
            if ($this->map[$x][$y] === 1) {
                $line[$x] = 0;
            } else {
                $line[$x] = isset($line[$x]) ? $line[$x] + 1 : 1;
            }
        }
        return $line;
    }

    protected function getHeight(int $x, int $y) {
        $height = 0;
        while ($y - $height >= 0 && $this->map[$x][$y - $height] === 0) {
            $height++;
        }
        return $height;
    }

    protected function rand() {
        srand(1234);
        for ($i = 0; $i < $this->n; $i++) {
            for ($j = 0; $j < $this->m; $j++) {
                $this->map[$i][$j] = (int)(rand(0, 5) / 4);
            }
        }
    }

    protected function printMap() {
        for ($i = 0; $i < $this->n; $i++) {
            $str = '';
            for ($j = 0; $j < $this->m; $j++) {
                $str .= $this->map[$i][$j] . ' ';
            }
            echo $str . PHP_EOL;
        }
    }
}