<?php

namespace MaxSquare;

class SolutionN2
{
    protected array $map = [];
    protected int $n = 10;
    protected int $m = 10;

    protected array $l;
    protected array $r;
    protected Stack $stack;

    public function __construct() {
        $this->rand();
        $this->printMap();

        $this->stack = new Stack();

        $sT = microtime(true);

        $r = $this->calc();

        echo 'result: ' . $r . PHP_EOL;
        echo 'time: ' . round(microtime(true) - $sT, 4) . PHP_EOL;

    }

    protected function calc() {
        $maxSquare = 0;
        $line = [];
        for ($y = 0; $y < $this->n; $y++) {
            $line = $this->getHeightLine($y, $line);
            $this->getRight($line);
            $this->getLeft($line);
            $square = $this->getMaxSquare($line);
            if ($maxSquare < $square) {
                $maxSquare = $square;
            }
        }
        return $maxSquare;
    }

    protected function getRight(array $line) {
        $stack = new Stack();
        for ($j = 0; $j < $this->n; $j++) {
            while (!$stack->isEmpty()) {
                if ($line[$j] < $line[$stack->pick()]) {
                    $this->r[$stack->pop()] = $j - 1;
                } else {
                    break;
                }
            }
            $stack->push($j);
        }
        while (!$stack->isEmpty()) {
            $this->r[$stack->pop()] = $this->n - 1;
        }
    }

    protected function getLeft(array $line) {
        $stack = new Stack();
        for ($j = $this->n - 1; $j >= 0; $j--) {
            while (!$stack->isEmpty()) {
                if ($line[$j] < $line[$stack->pick()]) {
                    $this->l[$stack->pop()] = $j + 1;
                } else {
                    break;
                }
            }
            $stack->push($j);
        }
        while (!$stack->isEmpty()) {
            $this->l[$stack->pop()] = 0;
        }
    }

    protected function getMaxSquare(array $line) {
        $square = 0;
        for ($x = 0; $x < $this->n; $x++) {
            $height = $line[$x];
            $weight = $this->r[$x] - $this->l[$x] + 1;
            $s = $height * $weight;
            if ($square < $s) {
                $square = $s;
            }
        }
        return $square;
    }

    protected function getHeightLine(int $y, array $line) {
        for ($x = 0; $x < $this->n; $x++) {
            if ($this->map[$x][$y] === 1) {
                $line[$x] = 0;
            } else {
                $line[$x] = isset($line[$x]) ? $line[$x] + 1 : 1;
            }
        }
        return $line;
    }

    protected function getHeight(int $x, int $y) {
        $height = 0;
        while ($y - $height >= 0 && $this->map[$x][$y - $height] === 0) {
            $height++;
        }
        return $height;
    }

    protected function rand() {
        srand(1234);
        for ($i = 0; $i < $this->n; $i++) {
            for ($j = 0; $j < $this->m; $j++) {
                $this->map[$i][$j] = (int)(rand(0, 5) / 4);
            }
        }
    }

    protected function printMap() {
        for ($i = 0; $i < $this->n; $i++) {
            $str = '';
            for ($j = 0; $j < $this->m; $j++) {
                $str .= $this->map[$i][$j] . ' ';
            }
            echo $str . PHP_EOL;
        }
    }
}